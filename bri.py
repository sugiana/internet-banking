import os
from time import sleep
from datetime import (
    date,
    timedelta,
    )
from PIL import Image
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import UnexpectedAlertPresentException
from helper_selenium import SeleniumApp
from captcha import read as ocr


CAPTCHA_FILE = 'tmp-bri-captcha.png'
LOGOUT_URL = 'https://ib.bri.co.id/ib-bri/Logout.html'
XPATH_USER = '//input[@placeholder="user ID"]'
XPATH_PASSWORD = '//input[@placeholder="password"]'
XPATH_CAPTCHA_INPUT = '//input[@name="j_code"]'
XPATH_CAPTCHA_IMG = '//img[@src="https://ib.bri.co.id/ib-bri/login/captcha"]'
JS_REKENING = "clickedParentMenuFix('defaultpg/default_page', "\
              "'MyAccount.html', 'myaccounts-fix')"
XPATH_REKENING = '//select[@id="ACCOUNT_NO"]/option'
XPATH_TAMPILKAN = '//input[@value="Tampilkan"]'

satu_hari = timedelta(1)

PERLU_LOGOUT = True


class App(SeleniumApp):
    start_url = 'https://ib.bri.co.id/ib-bri/Login.html'

    def login(self):  # Override
        if not os.path.exists('training'):
            os.mkdir('training')
        sleep(5)
        captcha_value = self.get_captcha()
        element = self.driver.find_element(By.XPATH, XPATH_USER)
        element.send_keys(self.username)
        element = self.driver.find_element(By.XPATH, XPATH_PASSWORD)
        element.send_keys(self.password)
        element = self.driver.find_element(By.XPATH, XPATH_CAPTCHA_INPUT)
        element.send_keys(captcha_value)
        sleep(15)
        new_value = element.get_attribute('value')
        if captcha_value == new_value:
            os.remove(CAPTCHA_FILE)
        else:
            new_file = f'{new_value}.png'
            new_file = os.path.join('training', new_file)
            os.rename(CAPTCHA_FILE, new_file)
        element.send_keys(Keys.ENTER)
        sleep(10)
        self.menu_rekening_clicked = False
        PERLU_LOGOUT and super().login()

    def logout(self):  # Override
        self.driver.get(LOGOUT_URL)
        sleep(5)
        try:
            webdriver.ActionChains(self.driver).send_keys(Keys.ENTER).perform()
        except UnexpectedAlertPresentException as e:
            print(e)
        sleep(5)
        super().logout()

    def get_captcha(self):
        element = self.driver.find_element(By.XPATH, XPATH_CAPTCHA_IMG)
        location = element.location
        size = element.size
        self.driver.save_screenshot(CAPTCHA_FILE)
        image = Image.open(CAPTCHA_FILE)
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        image = image.crop((left, top, right, bottom))
        image.save(CAPTCHA_FILE, 'png')
        return ocr(CAPTCHA_FILE)

    def menu_rekening(self, href):
        self.driver.switch_to.default_content()
        if not self.menu_rekening_clicked:
            self.driver.execute_script(JS_REKENING)
            sleep(10)
            self.menu_rekening_clicked = True
        self.driver.switch_to.frame(0)  # Frame sub-menu
        script = f'//a[@href="{href}"]'
        self.driver.find_element(By.XPATH, script).click()
        sleep(5)
        self.driver.switch_to.default_content()
        self.driver.switch_to.frame(1)  # Frame content

    def download_saldo(self):  # Override
        self.menu_rekening('BalanceInquiry.html')
        self.save('saldo.html')

    def set_date_element(self, name, value):
        e = self.driver.find_element(By.NAME, name)
        Select(e).select_by_value(value)

    def trx(self, rekening):
        e = self.driver.find_element(By.ID, 'ACCOUNT_NO')
        Select(e).select_by_value(rekening)
        dd = str(self.tgl.day).zfill(2)
        mm = str(self.tgl.month).zfill(2)
        yyyy = str(self.tgl.year)
        for i in (1, 2):
            self.set_date_element(f'DDAY{i}', dd)
            self.set_date_element(f'DMON{i}', mm)
            self.set_date_element(f'DYEAR{i}', yyyy)
        self.driver.find_element(By.XPATH, XPATH_TAMPILKAN).click()
        sleep(5)
        tgl_s = self.tgl.strftime('%Y-%m-%d')
        filename = f'trx-{rekening}-{tgl_s}.html'
        self.save(filename)

    def download_trx(self):  # Override
        self.menu_rekening('AccountStatement.html')
        rekening_list = []
        for xs in self.driver.find_elements(By.XPATH, XPATH_REKENING):
            rekening = xs.get_attribute('value')
            if not rekening:
                continue
            rekening_list.append(rekening)
        for rekening in rekening_list:
            self.trx(rekening)

    def run(self):
        if PERLU_LOGOUT:
            super().run()
            return
        self.before_download()
        # Tidak perlu logout agar tidak ketik captcha lagi
        while True:
            if 'saldo' in self.rencana:
                self.download_saldo()
                self.parse_saldo()
            if 'trx' in self.rencana:
                self.download_trx()
                self.parse_trx()
            self.menu_rekening_clicked = False
            sleep(60)
            if date.today() > self.tgl:
                self.tgl += satu_hari


if __name__ == '__main__':
    a = App()
    a.run()
