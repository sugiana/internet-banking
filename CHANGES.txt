2023-02-02
----------
- BRI kini ada pengisian captcha. User diberi kesempatan 10 detik untuk
  memperbaiki jika keliru. Bila ada perbaikan maka gambar captcha akan disimpan
  dengan nama file sesuai nilai yang diketik user. File ini sebagai bekal untuk
  pembelajaran oleh sistem. Proses pembelajarannya sendiri belum ada.

2023-01-23
----------
- Di Mandiri tutup popup sebelum login

2022-12-31
----------
- BCA perlu selenium

2022-05-15
----------
- Versi 2.0 menggunakan Python 3
- scrapy sebagai browser untuk web yang tidak menggunakan AJAX
- selenium sebagai pengendali browser Chrome untuk web yang menggunakan AJAX
- scrapy sebagai pemilah dengan output ke file CSV
- username dan password disimpan di file konfigurasi ketimbang di command line
  argument dengan alasan keamanan.

2014-07-18
----------
- Versi 1.0 menggunakan Python 2
- mechanize sebagai browser
- sgmllib sebagai parser (pemilah)
