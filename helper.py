import sys
import os
from datetime import datetime
from argparse import ArgumentParser
from configparser import ConfigParser
from datetime import date
from subprocess import call
from glob import glob
from common import mkdir


def run(cmd):
    print('DEBUG ' + ' '.join(cmd))
    if call(cmd) != 0:
        print('Ada kesalahan, akhiri.')
        sys.exit()


class BaseApp:
    def __init__(self, argv=sys.argv[1:]):
        pars = self.arg_parser()
        self.option = pars.parse_args(argv)
        self.conf = ConfigParser()
        self.conf.read(self.option.conf)
        self.bank = self.conf.get('main', 'bank')
        self.tmp_dir = self.conf.get('main', 'tmp_dir')
        self.rencana = self.conf.get('main', 'rencana').split(',')
        self.bank_dir = os.path.join(self.tmp_dir, self.bank)
        self.bin_path = os.path.split(sys.executable)[0]
        self.scrapy_bin = os.path.join(self.bin_path, 'scrapy')
        mkdir(self.tmp_dir)
        mkdir(self.bank_dir)
        if self.option.tgl:
            self.tgl = datetime.strptime(self.option.tgl, '%d-%m-%Y').date()
        else:
            self.tgl = date.today()
        print('DEBUG option.tgl', self.option.tgl)

    def arg_parser(self):
        tgl = date.today().strftime('%d-%m-%Y')
        help_tgl = f'contoh: {tgl}'
        pars = ArgumentParser()
        pars.add_argument('conf')
        pars.add_argument('--tgl', help=help_tgl)
        return pars

    def run_spider(self, spider_file, kwargs, output_file=None):
        spider_cmd = [self.scrapy_bin, 'runspider', spider_file]
        my_args = []
        for key in kwargs:
            val = kwargs[key]
            my_args += ['-a', f'{key}={val}']
        output_arg = output_file and ['-O', output_file] or []
        cmd = spider_cmd + my_args + output_arg
        run(cmd)

    def parse(self, filename):
        t = os.path.split(filename)
        name, ext = os.path.splitext(filename)
        ext_length = len(ext)
        csv_file = filename[:-ext_length] + '.csv'
        task = t[1][:-ext_length].split('-')[0]  # saldo / trx
        spider_file = f'spider/{self.bank}/{task}.py'
        d = dict(url=f'file://{filename}')
        self.run_spider(spider_file, d, csv_file)

    def download_saldo(self):  # Override, please
        pass

    def parse_saldo(self):
        saldo_html = os.path.join(self.bank_dir, 'saldo.html')
        self.parse(saldo_html)

    def download_trx(self):  # Override, please
        pass

    def parse_trx(self):
        tgl_s = self.tgl.strftime('%Y-%m-%d')
        pattern = os.path.join(self.bank_dir, f'trx*{tgl_s}.html')
        trx_files = glob(pattern)
        for trx_html in trx_files:
            self.parse(trx_html)

    def before_download(self):  # Selenium login
        pass

    def after_download(self):
        self.parse_saldo()
        self.parse_trx()

    def run(self):
        self.before_download()
        if 'trx' in self.rencana:
            self.download_trx()
        if 'saldo' in self.rencana:
            self.download_saldo()
        self.after_download()


class App(BaseApp):
    def run(self):  # Override
        tgl_s = self.tgl.strftime('%d-%m-%Y')
        filename = f'{self.bank}.py'
        cmd = [
                sys.executable, filename, self.option.conf,
                f'--tgl={tgl_s}']
        run(cmd)
        self.after_download()


if __name__ == '__main__':
    a = App()
    a.run()
