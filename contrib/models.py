from sqlalchemy import (
    Column,
    Integer,
    String,
    Text,
    Date,
    DateTime,
    Float,
    UniqueConstraint,
    )
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class TrxMixin:
    id = Column(Integer, primary_key=True)
    bank = Column(String(16), nullable=False)
    rekening = Column(String(16), nullable=False)
    urutan = Column(Integer, nullable=False)
    tgl = Column(Date, nullable=False)
    ket = Column(Text, nullable=False)
    nominal = Column(Float, nullable=False)
    saldo = Column(Float, nullable=False)
    __table_args__ = (
        UniqueConstraint('bank', 'rekening', 'tgl', 'urutan'),
        )


class SaldoMixin:
    id = Column(Integer, primary_key=True)
    bank = Column(String(16), nullable=False)
    rekening = Column(String(16), nullable=False)
    nominal = Column(Float, nullable=False)
    tgl_catat = Column(DateTime(timezone=True), nullable=False)
    __table_args__ = (
        UniqueConstraint('bank', 'rekening'),
        )
