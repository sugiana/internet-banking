import sys
import os
from configparser import ConfigParser
from argparse import ArgumentParser
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import zope.sqlalchemy


class BaseApp:
    def __init__(self, argv=sys.argv[1:]):
        pars = self.arg_parser()
        self.option = pars.parse_args(argv)
        bank_dir = os.path.split(self.option.csv_file)[0]
        self.bank = os.path.split(bank_dir)[1].upper()
        conf = ConfigParser()
        conf.read(self.option.conf)
        db_url = conf.get('main', 'db_url')
        engine = create_engine(db_url)
        session_factory = sessionmaker(bind=engine)
        self.db_session = session_factory()
        zope.sqlalchemy.register(self.db_session)

    def arg_parser(self):
        pars = ArgumentParser()
        pars.add_argument('conf')
        pars.add_argument('--csv-file', required=True)
        return pars
