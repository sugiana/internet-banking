import os
import csv
from time import localtime
from datetime import datetime
import pytz
import transaction
from common import BaseApp


def file_time(filename, tz):
    s = os.stat(filename)
    t = localtime(s.st_mtime)
    return datetime(
            t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec,
            tzinfo=tz)


class BaseSaldo(BaseApp):
    # ORM turunan SaldoMixin
    orm = None  # Override, please
    timezone = 'Asia/Jakarta'

    def run(self):
        tz = pytz.timezone(self.timezone)
        tgl = file_time(self.option.csv_file, tz)
        with open(self.option.csv_file) as f:
            c = csv.DictReader(f)
            for r in c:
                rekening = r['rekening']
                nominal = float(r['nominal'])
                print(f'{rekening} {nominal}')
                q = self.db_session.query(self.orm).filter_by(
                        bank=self.bank, rekening=rekening)
                row = q.first()
                if row:
                    row.nominal = nominal
                    row.tgl_catat = tgl
                else:
                    d = dict(
                            bank=self.bank, rekening=rekening, nominal=nominal,
                            tgl_catat=tgl)
                    row = self.orm(**d)
                with transaction.manager:
                    self.db_session.add(row)
