import sys
import os
from datetime import date
from argparse import ArgumentParser
from glob import glob
from subprocess import call


def run(cmd):
    print(' '.join(cmd))
    if call(cmd) != 0:
        sys.exit()


pars = ArgumentParser()
pars.add_argument('conf')
pars.add_argument('--csv-dir', required=True)
option = pars.parse_args(sys.argv[1:])

tgl = date.today()
tgl_s = tgl.strftime('%Y-%m-%d')
pattern = os.path.join(option.csv_dir, f'trx*{tgl_s}.csv')
for csv_file in glob(pattern):
    cmd = [sys.executable, 'example-trx-db.py', option.conf,
           f'--csv-file={csv_file}']
    run(cmd)

csv_file = os.path.join(option.csv_dir, f'saldo.csv')
if os.path.exists(csv_file):
    cmd = [sys.executable, 'example-saldo-db.py', option.conf,
           f'--csv-file={csv_file}']
    run(cmd)
