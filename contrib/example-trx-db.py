from sqlalchemy.ext.declarative import declarative_base
from models import TrxMixin
from trx_db import BaseTrx


Base = declarative_base()


class Trx(Base, TrxMixin):
    __tablename__ = 'mutasi'
    __table_args__ = dict(schema='bank')


class App(BaseTrx):
    orm = Trx  # Override

    def arg_parser(self):  # Override
        pars = super().arg_parser()
        pars.add_argument('--init-db', action='store_true')
        return pars


a = App()
if a.option.init_db:
    Base.metadata.create_all(a.db_session.bind)
a.run()
