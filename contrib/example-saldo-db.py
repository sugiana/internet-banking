from sqlalchemy.ext.declarative import declarative_base
from models import SaldoMixin
from saldo_db import BaseSaldo


Base = declarative_base()


class Saldo(Base, SaldoMixin):
    __tablename__ = 'saldo'
    __table_args__ = dict(schema='bank')


class App(BaseSaldo):
    orm = Saldo  # Override

    def arg_parser(self):  # Override
        pars = super().arg_parser()
        pars.add_argument('--init-db', action='store_true')
        return pars


a = App()

if a.option.init_db:
    Base.metadata.create_all(a.db_session.bind)
a.run()
