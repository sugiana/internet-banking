import csv
from datetime import datetime
from sqlalchemy import func
import transaction
from common import BaseApp


class BaseTrx(BaseApp):
    # ORM turunan TrxMixin
    orm = None  # Override, please

    def run(self):
        with open(self.option.csv_file) as f:
            c = csv.DictReader(f)
            for r in c:
                rekening = r['rekening']
                ket = r['ket']
                tgl = datetime.strptime(r['tgl'], '%d-%m-%Y').date()
                nominal = float(r['nominal'])
                saldo = float(r['saldo'])
                print(f'{rekening} {tgl} {ket} {nominal} {saldo}')
                q = self.db_session.query(self.orm).filter_by(
                        bank=self.bank, rekening=rekening, nominal=nominal,
                        saldo=saldo, ket=ket, tgl=tgl)
                trx = q.first()
                if trx:
                    continue
                q = self.db_session.query(
                        func.max(self.orm.urutan)).filter_by(
                            bank=self.bank, rekening=rekening, tgl=tgl)
                urutan = q.scalar() or 0
                urutan += 1
                trx = self.orm(
                        bank=self.bank, rekening=rekening, tgl=tgl,
                        urutan=urutan, ket=ket, nominal=nominal, saldo=saldo)
                with transaction.manager:
                    self.db_session.add(trx)
