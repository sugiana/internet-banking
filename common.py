import os
from datetime import datetime
import urllib.parse as urlparse
from configparser import ConfigParser
from scrapy import (
    Spider,
    FormRequest,
    Request,
    signals,
    )


def mkdir(path):
    if not os.path.exists(path):
        os.mkdir(path)


class BaseCrawler(Spider):
    logged_in = False
    start_urls = []  # Override, please

    def __init__(self, conf_file, tgl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.conf = ConfigParser()
        self.conf.read(conf_file)
        self.username = self.conf.get('main', 'username')
        self.password = self.conf.get('main', 'password')
        self.tmp_dir = self.conf.get('main', 'tmp_dir')
        mkdir(self.tmp_dir)
        self.tmp_dir = os.path.join(self.tmp_dir, self.name)
        mkdir(self.tmp_dir)
        self.tgl = tgl and datetime.strptime(tgl, '%d-%m-%Y')

    def idle(self):
        if self.logged_in:
            req = self.req_logout()
            self.crawler.engine.crawl(req)

    def req_logout(self):  # Override, please
        return

    def logout_if_idle(self):
        self.crawler.signals.connect(self.idle, signal=signals.spider_idle)

    def req(self, path, callback=None, params=dict(), method='GET'):
        path = path.lstrip('/')
        url = '/'.join([self.start_urls[0], path])
        if method == 'POST':
            if params:
                return FormRequest(
                        url, formdata=params, callback=callback)
            return Request(url, method=method, callback=callback)
        if params:
            qs = urlparse.urlencode(params)
            sep = url.find('?') > -1 and '&' or '?'
            url = sep.join([url, qs])
        return Request(url, method=method, callback=callback)

    def save_response(self, response, filename):
        full_path = os.path.join(self.tmp_dir, filename)
        with open(full_path, 'wb') as f:
            f.write(response.body)
        self.logger.debug(f'{full_path} tersimpan.')
