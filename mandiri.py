from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from helper_selenium import SeleniumApp


class App(SeleniumApp):
    start_url = 'https://ibank.bankmandiri.co.id/retail3/loginfo/loginRequest'

    def login(self):  # Override
        # Tutup popup
        webdriver.ActionChains(self.driver).send_keys(Keys.ESCAPE).perform()
        self.driver.find_element(By.ID, 'userid_sebenarnya').send_keys(
            self.username)
        self.driver.find_element(By.ID, 'pwd_sebenarnya').send_keys(
            self.password)
        self.driver.find_element(By.ID, 'btnSubmit').click()
        sleep(5)  # Tunggu Javascript bekerja
        self.save('saldo.html')
        super().login()

    def logout(self):  # Override
        self.driver.find_element(By.CLASS_NAME, 'mdr-logout').click()
        sleep(1)  # Tunggu Javascript bekerja
        self.driver.find_element(By.ID, 'btnCancelReg').click()
        super().logout()

    def set_date(self, label, tgl_str):
        tgl_input = self.driver.find_element(By.ID, label)
        script = "arguments[0].setAttribute('value', arguments[1])"
        self.driver.execute_script(script, tgl_input, tgl_str)

    def trx(self, rekening):
        tgl_s = self.tgl.strftime('%Y-%m-%d')
        filename = f'trx-{rekening}-{tgl_s}.html'
        tgl_s = self.tgl.strftime('%d/%m/%Y')
        element_id = f'currentId-{rekening}'
        script = f'//a[contains(@id,"{element_id}")]'\
                 '//div[contains(@class,"acc-left")]'
        self.driver.find_element(By.XPATH, script).click()
        sleep(5)
        self.set_date('fromDate', tgl_s)
        self.set_date('toDate', tgl_s)
        self.driver.find_element(By.ID, 'btnSearch').click()
        sleep(5)
        self.save(filename)

    def download_trx(self):  # Override
        rekening_list = []
        script = f'//a[contains(@id,"currentId-")]'
        for xs in self.driver.find_elements(By.XPATH, script):
            rekening = xs.get_attribute('id')
            rekening = rekening.lstrip('currentId-')
            rekening_list.append(rekening)
        i = 0
        count = len(rekening_list)
        for rekening in rekening_list:
            i += 1
            self.trx(rekening)
            self.logout()
            if i < count:
                self.login()


if __name__ == '__main__':
    a = App()
    a.run()
