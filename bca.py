from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from helper_selenium import SeleniumApp


BASE_URL = 'https://ibank.klikbca.com'
LOGOUT_URL = BASE_URL + '/authentication.do?value(actions)=logout'
JS_SALDO = "goToPage('balanceinquiry.do')"
JS_TRX = "goToPage('accountstmt.do?value(actions)=acct_stmt')"
XPATH_TAMPILKAN = '//input[@name="value(submit1)"]'


class App(SeleniumApp):
    start_url = BASE_URL

    def login(self):  # Override
        self.driver.find_element(By.ID, 'user_id').send_keys(
            self.username)
        self.driver.find_element(By.ID, 'pswd').send_keys(
            self.password)
        self.driver.find_element(By.NAME, 'value(Submit)').click()
        super().login()

    def logout(self):  # Override
        self.driver.get(LOGOUT_URL)
        super().logout()

    def download_saldo(self):  # Override
        self.driver.switch_to.frame(1)
        self.driver.execute_script(JS_SALDO)
        sleep(2)
        self.driver.switch_to.default_content()
        self.driver.switch_to.frame(2)
        self.save('saldo.html')

    def select_element(self, name, value):
        e = self.driver.find_element(By.NAME, name)
        Select(e).select_by_value(value)

    def download_trx(self):  # Override
        self.driver.switch_to.frame(1)
        self.driver.execute_script(JS_TRX)
        self.driver.switch_to.default_content()
        self.driver.switch_to.frame(2)
        dd = str(self.tgl.day).zfill(2)
        mm = str(self.tgl.month)
        yyyy = str(self.tgl.year)
        self.select_element('value(startDt)', dd)
        self.select_element('value(startMt)', mm)
        self.select_element('value(startYr)', yyyy)
        self.select_element('value(endDt)', dd)
        self.select_element('value(endMt)', mm)
        self.select_element('value(endYr)', yyyy)
        self.driver.find_element(By.XPATH, XPATH_TAMPILKAN).click()
        sleep(2)
        tgl_s = self.tgl.strftime('%Y-%m-%d')
        filename = f'trx-{tgl_s}.html'
        self.save(filename)
        self.driver.switch_to.default_content()


if __name__ == '__main__':
    a = App()
    a.run()
