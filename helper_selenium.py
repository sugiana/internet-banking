import os
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from helper import BaseApp


class SeleniumApp(BaseApp):
    options_class = Options
    start_url = None  # Override, please

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        opt = self.options_class()
        if self.option.browser_tidak_tampak:
            opt.add_argument("--headless")
        self.set_driver(opt)
        self.username = self.conf.get('main', 'username')
        self.password = self.conf.get('main', 'password')
        self.logged_in = False

    def set_driver(self, opt):
        driver_manager = ChromeDriverManager()
        service = Service(driver_manager.install())
        self.driver = webdriver.Chrome(service=service, options=opt)

    def arg_parser(self):  # Override
        pars = super().arg_parser()
        pars.add_argument('--browser-tidak-tampak', action='store_true')
        return pars

    def save(self, filename):
        full_path = os.path.join(self.bank_dir, filename)
        with open(full_path, 'w') as f:
            f.write(self.driver.page_source)
        print(f'File {full_path} tersimpan.')

    def home(self):
        self.driver.get(self.start_url)
        sleep(5)  # Tunggu Javascript selesai bekerja

    def login(self):  # Override, please
        self.logged_in = True

    def logout(self):  # Override, please
        self.logged_in = False

    def before_download(self):  # Override
        self.home()
        self.login()

    def after_download(self):  # Override
        if self.logged_in:
            self.logout()
        self.driver.quit()
