import cv2
import pytesseract


def read(filename):
    img = cv2.imread(filename)
    custom_config = r'--oem 3 --psm 6'
    s = pytesseract.image_to_string(img, config=custom_config)
    return s.strip()
