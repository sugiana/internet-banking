Internet Banking Crawler
========================

Pembaca saldo dan mutasi transaksi situs Internet banking. Bank yang sudah
didukung adalah:

1. BCA
2. Mandiri
3. BRI

Tahapan umum yang dilakukan *script pengunduh* adalah:

1. Buka halaman login.
2. Masukkan username dan password.
3. Klik login.
4. Ke menu saldo.
5. Simpan halamannya ke file HTML.
6. Logout.

Selanjutnya *script pemilah* isi file HTML tadi dijalankan.

Ketiga bank itu menggunakan Javascript tingkat lanjut (AJAX) sehingga
membutuhkan web browser sesungguhnya yaitu Chrome. Oleh karena itu script
Mandiri harus dijalankan di lingkungan desktop (Graphical User Interface /
GUI).


Python Virtual Environment
--------------------------

Ini membutuhkan Python versi 3.6 ke atas. Buatlah *virtual environment*::

    $ python3.10 -m venv ~/env
    $ ~/env/bin/pip install --upgrade pip wheel

Pasang paket yang dibutuhkan::

    $ ~/env/bin/pip install -r requirements.txt


BCA
---

Siapkanlah file konfigurasi bernama ``live-bca.ini``::

    [main]
    bank = bca
    rencana = saldo,trx
    username = myusername
    password = mypassword
    tmp_dir = /home/sugiana/tmp

Berikut penjelasannya:

1. Nilai ``bank`` akan digunakan untuk menjalankan file pengunduh dan pemilah. 
2. ``rencana`` adalah tindakan yang akan dilakukan. Setiap tindakan dipisahkan
   dengan koma. ``saldo`` untuk cek saldo dan ``trx`` untuk mutasi transaksi. 
3. ``username`` dan ``password`` untuk login ke web internet banking.
4. ``tmp_dir`` akan digunakan script pengunduh untuk menyimpan file HTML. Juga
   digunakan pemilah untuk menyimpan file CSV.
   
Kini jalankan::

    $ ~/env/bin/python helper.py live-bca.ini

File yang akan terbentuk adalah:

1. ``tmp/bca/saldo.html``
2. ``tmp/bca/saldo.csv``
3. ``tmp/bca/trx-2022-12-30.html``
4. ``tmp/bca/trx-2022-12-30.csv``

Untuk ``trx`` default-nya adalah tanggal hari ini. Untuk tanggal sebelumnya::

    $ ~/env/bin/python helper.py live-bca.ini --tgl=13-5-2022

Di sistem *input transfer otomatis* biasanya tidak memerlukan ``saldo`` dalam
``rencana``. Maka di file konfigurasi cukup ditulis::

    rencana = trx

agar menghemat waktu.

**Hati-hati dengan transaksi hari Sabtu** yang akan muncul kembali di hari
Minggu dan Senin. Meski secara file tertulis tanggal hari Minggu seperti
``trx-2022-05-15.csv`` namun isinya memuat tanggal hari Sabtu::

    rekening,tgl,ket,nominal,saldo
    2291440381,14-05-2022,TRSF E-BANKING DB 05/14 95031 NASI UDUK

Untungnya urutan transaksi konsisten. Jadi secara database kita bisa menetapkan
keunikan ini berdasarkan bank, rekening, tanggal, dan *nomor urut per tanggal*.


Mandiri
-------

Mirip BCA, siapkanlah file konfigurasi bernama ``live-mandiri.ini``::

    [main]
    bank = mandiri
    rencana = trx
    username = myusername
    password = mypassword
    tmp_dir = /home/sugiana/tmp

Untuk menjalankannya masih mirip::

    $ ~/env/bin/python helper.py live-mandiri.ini

Halaman awal setelah login berisi saldo. Dengan demikian kita tidak perlu
menambahkan ``saldo`` dalam ``rencana``, kecuali jika hanya ingin cek saldo
saja tanpa unduh mutasi transaksi::

    rencana = saldo

Jika Anda akan menyimpan informasi ini di database yang berada di
*remote server* maka siapkanlah:

1. PC desktop di rumah atau kantor
2. Script pengunggah file CSV
3. Web service yang akan menerima file CSV

Skenario di PC desktop sebagai berikut:

1. Jalankan ``helper.py``
2. Jalankan script pengunggah file CSV melalui web service tadi.

Sedangkan web service di remote server akan menyimpan isi file CSV ke database.


BRI
---

BRI menggunakan *captcha* saat login. Alurnya begini:

1. Script menampilkan form login
2. Script mengisi username, password, dan captcha
3. Script menunggu 10 detik untuk memberi kesempatan user memperbaiki nilai captcha jika salah
5. Script menekan Enter untuk login
6. Script membaca saldo dan mutasi transaksi
7. Script melakukan logout

Seperti biasa buatlah file konfigurasi bernama ``live-bri.ini``::

    [main]
    bank = bri 
    rencana = saldo,trx
    username = myusername
    password = mypassword
    tmp_dir = /home/sugiana/tmp

Jalankan::

    $ ~/env/bin/python helper.py live-bri.ini

Tunggulah sampai username, password, dan captcha tertulis. Perbaiki nilai
captcha bila perlu. Adapun letak file CSV-nya nanti mirip dengan BCA maupun
Mandiri.

Jika Anda mendapatkan pesan kesalahan terkait network maka tunggu sekitar 15 
menit untuk mencobanya lagi.

Agar tidak logout Anda perlu mengubah ``PERLU_LOGOUT`` menjadi ``False``
sehingga tidak perlu lagi mengawasi captcha. Bila Anda menambahkan opsi
``--tgl`` maka yang diunduh tidak hanya transaksi tanggal yang dimaksud
melainkan **juga tanggal setelahnya hingga hari ini**. Contoh::

    $ ~/env/bin/python helper.py live-bri.ini --tgl=19-5-2022

Bila sekarang 22 Mei 2022 maka yang diunduh adalah tanggal 19, 20, 21, dan 22.
Untuk menit-menit berikutnya yang diunduh hanya tanggal hari ini saja. Jadi
jangan khawatir bila lewat tengah malam. Perlakuan ini memang berbeda dengan
BCA dan Mandiri karena menghindari captcha tadi.

Sebenarnya BRI tidak ramah crawler bukan hanya melalui captcha. Ia juga tidak
memperkenankan user untuk login dalam 5 jam ke depan bila terlalu sering.


Developer
---------

Bila Anda ingin menambahkan bank lainnya maka yang perlu diperhatikan adalah
apakah web-nya menerapkan AJAX? Bila iya maka butuh ``selenium``. Bila tidak
maka pengunduh cukup dengan ``scrapy``. Misalkan BNI yang tampak **membutuhkan
AJAX** maka nama-nama script yang perlu disiapkan adalah:

1. ``bni.py`` sebagai pengunduh
2. ``spider/bni/saldo.py`` sebagai pemilah saldo
3. ``spider/bni/trx.py`` sebagai pemilah mutasi transaksi

Kemudian di file konfigurasi ``live-bni.ini``::

    [main]
    bank = bni
    rencana = saldo,trx
    username = myusername
    password = mypassword
    tmp_dir = /home/sugiana/tmp

Lalu awali ``bni.py`` dengan script sederhana ini::

    from helper_selenium import SeleniumApp


    class App(SeleniumApp):
        start_url = 'https://ibank.bni.co.id'


    if __name__ == '__main__':
        a = App()
        a.run()

Selama mempelajari teknik unduhnya tentu kita belum bisa membuat file
``saldo.py`` dan ``trx.py`` karena file HTML belum ada. Jadi jalankan script
tanpa melalui ``helper.py`` dulu::

    $ ~/env/bin/python bni.py live-bni.ini

Kita juga bisa menyertakan opsi ``--tgl`` untuk tanggal transaksi sebelumnya.

Untuk membuat versi ``selenium`` ini akan lebih nyaman bila berlangsung di moda
interaktif Python::

    $ ~/env/bin/python

    >>> from bni import App
    >>> a = App(['live-bni.ini'])

Sampai di sini browser akan tampil dengan halaman kosong. Lanjutkan::

    >>> a.run()

Ini akan menampilkan form login. Kita bisa menyimpan HTML-nya untuk dipelajari
lebih lanjut::

    >>> a.save('welcome.html')

Ini akan tersimpan di ``/home/sugiana/tmp/bni/welcome.html``, tentunya sesuai
dengan file konfigurasi. Bila di sini ada Python error maka itu artinya web BNI
lebih rumit lagi dari BRI :)


Non AJAX
--------

Lalu misalkan Bank Permata setelah dianalisa **tidak membutuhkan AJAX** maka
yang perlu disiapkan:

1. ``permata_spider.py`` sebagai pengunduh. Akhiran nama ``_spider`` untuk
   memberitahukan ``helper.py`` bahwa file ini dijalankan oleh
   ``bin/scrapy runspider``.
2. ``spider/permata/saldo.py`` sebagai pemilah saldo
3. ``spider/permata/trx.py`` sebagai pemilah mutasi transaksi

Siapkan file konfigurasi ``live-permata.ini``::

    [main]
    bank = permata
    rencana = saldo,trx
    username = myusername
    password = mypassword
    tmp_dir = /home/sugiana/tmp

Lalu buat file ``permata_spider.py``::

    from common import BaseCrawler


    class Crawler(BaseCrawler):
        name = 'permata'
        start_urls = ['https://www.permatae-business.com']

        def parse(self, response):  # Override
            self.save_response(response, 'welcome.html')

Jalankan::

    $ ~/env/bin/scrapy runspider permata_spider.py -a conf_file=live-permata.ini

Bila sudah berhasil mengunduh maka untuk memudahkan uji coba pemilahan bisa
gunakan perintah ini::

    $ ~/env/bin/scrapy shell /home/sugiana/tmp/permata/welcome.html

Tentu saja kita juga bisa melihatnya dengan text editor biasa seperti ``vi``.

Selamat mencoba.
