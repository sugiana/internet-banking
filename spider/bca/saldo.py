from scrapy import Spider


def plain_num(s):
    return s.replace(',', '').lstrip()


class Saldo(Spider):
    name = 'saldo'

    def __init__(self, url, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_urls = [url]  # file:///home/sugiana/tmp/bca/saldo.html

    def parse(self, response):  # Override
        first = True
        for xs_tr in response.xpath('//table/tbody/tr'):
            if first:
                first = False
                continue
            r = []
            for xs_td in xs_tr.xpath('td/div/font/text()'):
                value = xs_td.get().strip()
                if not value:
                    break
                r.append(value)
            if not r:
                continue
            self.logger.debug(r)
            rekening, a, currency, saldo = r
            nominal = plain_num(saldo)
            yield dict(rekening=rekening, currency=currency, nominal=nominal)
