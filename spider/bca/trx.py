import re
from datetime import (
    date,
    datetime,
    )
from scrapy import Spider


POLA = [
    # KARTU DEBIT 27/05 AAAA,BBBB
    ['DEBIT ([0-9][0-9])/([0-9][0-9])', 1, 2],
    # Ainul: kalau ada 95031 berarti mm/dd
    # TRSF E-BANKING CR 01/04 95031 AAAA BBBB -> 4 Januari
    [' ([0-9][0-9])/([0-9][0-9]) 95031', 2, 1],
    # TRSF E-BANKING DB 06/24 79021 AAAA BBBB
    [' ([0-9][0-9])/([0-9][0-9]) 79021', 2, 1],
    # TRSF E-BANKING DB 04/19 79011 AAAA BBBB
    [' ([0-9][0-9])/([0-9][0-9]) 79011', 2, 1],
    # TRSF E-BANKING DB 07/17 77981 AAAA BBBB
    [' ([0-9][0-9])/([0-9][0-9]) 77981', 2, 1],
    # TRSF E-BANKING DB 08/12 84601 AAAA BBBB
    # TRSF E-BANKING DB TANGGAL NULL/08 08/08  WSID:59611 AAAA BBBB
    ['DB ([0-9][0-9])/([0-9][0-9])', 1, 2],
    # TRSF E-BANKING CR 0706/FTSCY/WS95011 200183.00 AAAA BBBB
    # TRSF E-BANKING DB 1408/FTSCY/WS95011 300300.00 AAAA BBBB
    ['(DB|CR) ([0-9][0-9])([0-9][0-9])', 2, 3],
    # SWITCHING CR TANGGAL :07/06    TRANSFER   DR 002 AAAA BBBB
    [':([0-9][0-9])/([0-9][0-9])', 1, 2],
    ]


def ket2tgl(s):
    for regex, ud, um in POLA:
        match = re.compile(regex).search(s)
        if match:
            return int(match.group(ud)), int(match.group(um))
    return None, None


def tgl_sebenar(tgl_str, ket, tgl_catat):
    day, month = ket2tgl(ket)
    if day:
        tgl_ket = date(tgl_catat.year, month, day)
        jeda = tgl_catat - tgl_ket
        if abs(jeda.days) > 29:
            return date(tgl_catat.year, day, month)
        return tgl_ket
    if tgl_str == 'PEND':
        return tgl_catat
    day, month = tgl_str.split('/')  # 08/06 -> 8 Juni
    return date(tgl_catat.year, int(month), int(day))


def plain_num(s):
    return s.replace(',', '').lstrip()


def parse_header(response):
    d = dict()
    for xs_tr in response.xpath('//table/tbody/tr'):
        r = []
        for xs_td in xs_tr.xpath('td/font/text()'):
            value = xs_td.get()
            r.append(value)
        if len(r) == 3:
            label = r[0]
            value = r[2]
            d[label] = value
    if 'Nomor Rekening' not in d:
        return
    t = d['Periode'].split(' - ')
    d['tgl'] = datetime.strptime(t[0], '%d/%m/%Y')
    d['tgl'] = d['tgl'].date()
    return d


class Trx(Spider):
    name = 'trx'

    def __init__(self, url, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_urls = [url]

    def parse(self, response):  # Override
        header = parse_header(response)
        if not header:
            self.logger.error('Tidak ada transaksi')
            return
        rekening = header['Nomor Rekening']
        for xs_tr in response.xpath('//table/tbody/tr'):
            r = []
            for xs_td in xs_tr.xpath('td/div/font'):
                values = xs_td.xpath('text()').extract()
                values = [x.strip() for x in values]
                value = '\n'.join(values)
                if not value:
                    break
                r.append(value)
            if not r:
                continue
            tgl, ket, _, nominal, kode, saldo = r
            ket = ket.replace('\n', ' ')
            tgl = tgl_sebenar(tgl, ket, header['tgl'])
            tgl = tgl.strftime('%d-%m-%Y')
            nominal = plain_num(nominal)
            if kode != 'CR':
                nominal = '-' + nominal
            saldo = plain_num(saldo)
            yield dict(
                    rekening=rekening, tgl=tgl, ket=ket, nominal=nominal,
                    saldo=saldo)
