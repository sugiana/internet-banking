from scrapy import Spider


def get_text(xs):
    s = xs.xpath('text()').extract()
    return s and s[0] or ''


def parse_nominal(xs):
    s = get_text(xs)
    return s and s.replace('.', '').replace(',', '.')


def get_nominal(xs_d, xs_k):
    d = parse_nominal(xs_d)
    k = parse_nominal(xs_k)
    sign = d and '-' or ''
    n = d or k
    return sign + n


class Trx(Spider):
    name = 'trx'

    def __init__(self, url, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_urls = [url]

    def parse(self, response):  # Override
        script = '//table[contains(@class, "rekkor")]/tbody/tr'
        xs = response.xpath(script)
        rekening = xs[1].xpath('td/text()')[1].extract().lstrip()
        script = '//table[@id="tabel-saldo"]/tbody/tr'
        for xs in response.xpath(script)[1:-2]:
            tgl, ket, debit, kredit, saldo = xs.xpath('td')
            d, m, y = get_text(tgl).split('/')
            tgl = f'{d}-{m}-20{y}'
            ket = get_text(ket)
            nominal = get_nominal(debit, kredit)
            saldo = parse_nominal(saldo)
            yield dict(
                    rekening=rekening, tgl=tgl, ket=ket, nominal=nominal,
                    saldo=saldo)
