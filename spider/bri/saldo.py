from scrapy import Spider


class Saldo(Spider):
    name = 'saldo'

    def __init__(self, url, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_urls = [url]

    def parse(self, response):  # Override
        xs = response.xpath('//table/tbody/tr')
        for s in xs:
            rekening = s.xpath('td/text()')[0].extract().rstrip()
            currency = s.xpath('td/text()')[3].extract().rstrip()
            nominal = s.xpath('td/text()')[4].extract()
            nominal = nominal.replace('.', '').replace(',', '.')
            nominal = nominal.rstrip()
            yield dict(
               rekening=rekening, currency=currency, nominal=nominal)
