from scrapy import Spider


def parse_nominal(xs):
    s = xs.xpath('text()').extract()
    if s:
        return s[0]
    return xs.xpath('span/text()').extract()[0]


def get_nominal(xs_d, xs_k):
    d = parse_nominal(xs_d)
    k = parse_nominal(xs_k)
    n = d == '-' and k or d
    sign = k == '-' and '-' or ''
    val = sign + n.replace(',', '')
    return float(val)


class Trx(Spider):
    name = 'trx'

    def __init__(self, url, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_urls = [url]

    def parse(self, response):  # Override
        script = '//span[@class="accountNoVal"]/text()'
        rekening = response.xpath(script).extract()[0].strip()
        tgl = response.xpath('//input[@id="fromDate"]').re('value="(.*)"')[0]
        tahun = tgl.split('/')[-1]
        script = '//label[@id="beginingBalance"]/text()'
        saldo = response.xpath(script).extract()[0].replace(',', '')
        saldo = float(saldo)
        script = '//table[@id="globalTable"]//tbody//tr'
        rows = []
        for xs in response.xpath(script):
            fields = xs.xpath('td')
            if len(fields) != 4:
                break
            tgl, ket, debit, kredit = fields
            tgl = tgl.xpath('text()').extract()[0]
            tgl = tgl.replace('/', '-') + '-' + tahun
            ket = ket.xpath('div/text()').extract()[0].strip()
            nominal = get_nominal(debit, kredit)
            rows.append(
                    dict(rekening=rekening, tgl=tgl, ket=ket, nominal=nominal))
        # Di Mandiri yang paling atas adalah yang terbaru jadi perlu dibalik
        # agar urutannya benar.
        rows.reverse()
        for row in rows:
            saldo += row['nominal']
            row['saldo'] = saldo
            yield row
