from scrapy import Spider


class Saldo(Spider):
    name = 'saldo'

    def __init__(self, url, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_urls = [url]

    def parse(self, response):  # Override
        xs = response.xpath('//div[@class="acc-right"]/div')
        acc_list = xs.re('id="(.*)" class')
        curr_list = xs.xpath('p/span/text()').extract()
        nominal_list = []
        for s in xs.xpath('p').extract():
            awal = s.find('span>') + 5
            akhir = s.find('<sup')
            nominal = s[awal:akhir].replace('.', '')
            nominal_list += [nominal]
        i = -1
        for dec in xs.xpath('p/sup/text()').extract():
            i += 1
            nominal = nominal_list[i] + '.' + dec
            yield dict(
               rekening=acc_list[i], currency=curr_list[i], nominal=nominal)
